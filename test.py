import boto3
import sys

s3 = boto3.client('s3')

response = s3.list_buckets()

buckets = [bucket['Name'] for bucket in response['Buckets']]

print('Bucket List: %s' % buckets)

print('Creating a new bucket...')

filename = 'README.md'
bucket_name = 'my-bucket-from-python'

s3.create_bucket(Bucket=bucket_name)
s3.upload_file(filename, bucket_name, filename)

