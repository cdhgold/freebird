#!/usr/bin/env bash
#Created by: Chris Goldsmith
#Created on:5/31/2018
#kill_all_task and update_task_count functions intially created by Michele Fenech Adami 
#Purpose: Allow updating of AWS ECS Container task via script
#Version 1.0 - Original Draft
#Version 1.2 - cleaned up variables, comments, added error checking on parameters

USAGE=$1
REGION=$2
ECS_CLUSTER=$3
TASK_COUNT=$4
SERVICE=$5

#Force Stops the tasks of a given cluster and service. 
#Added since without this AWS will take ~10 mins until it drains all connections and kills the container
kill_all_tasks() {
	echo "killing all tasks"
	tasks=$(aws ecs list-tasks --cluster $ECS_CLUSTER --service-name $SERVICE | jq '.[] | .[]' )
	for key in ${tasks[@]}; do
		echo "Force Stopping task $key"
		tasks=$(aws ecs stop-task --cluster $ECS_CLUSTER --task $key)
	done
}

#Updates the task count
update_task_count() {
	#Updates the task count to a value passed in a parameter
	#We then wait for the tasks to update, before reporting a success/ failure. 

	echo "Updating the $SERVICE task count to $TASK_COUNT"

	Desired_Count=$(aws ecs update-service --region $REGION --cluster $ECS_CLUSTER --service $SERVICE --desired-count $TASK_COUNT | jq '.service' | jq '.desiredCount')

	if [ "$Desired_Count" == "$TASK_COUNT" ]; then
		echo "Desired count has been updated. Will wait for tasks to update."
	else 
		echo "Error: Desired count not set to $TASK_COUNT"
		exit 1

	fi 	

	#Attempts to wait for the tasks to reach the desired state. 
	for attempt in {1..30}; do
        running_count=$(aws ecs describe-services --region $REGION --cluster $ECS_CLUSTER --services  $SERVICE | jq .services[0].deployments | jq .[0].runningCount)
			
		if [ $running_count -ne $TASK_COUNT ]; then
			echo "Waiting for tasks to update....."

			if [[ $Desired_Count -eq 0 ]]; then
				kill_all_tasks $TASK_COUNT

			fi
			sleep 5
		else 
			echo "Task(s) has been updated"
			return 
        fi
    done
	echo "Timeout, tasks have taken too long to update"
	exit 1	

}

help() {
        echo "================================================================================================="
        echo " -l = list running services on the cluster" 
	echo "		required parameter are region and cluster name"
        echo "		example: manage_ecs_containers.sh -l us-east-1 dev04"
        echo " -u = update task count to provided value for specfied service"
        echo "		required parameters are region, cluster name, task count and service_name"
	echo "		example: manage_ecs_containers.sh -u us-east-1 dev04 1 cdhgold"
        echo " -n = nuke all services by setting task count to 0 in prep for cluster reboot"
	echo "		required parameter are region and cluster name"
	echo "		manage_ecs_containers.sh -n us-east-1 dev04"
        echo " -h = this help display"
        echo "================================================================================================="
#        exit 0
}

if [ -z  $1 ] || [ -z $2 ] || [ -z $3 ] ; then
	echo "Missing Required Parameter"
	echo "==============================="
	echo "Parameter #1 - Operation witch"
	echo "Parameter #2 - Region: us-east-1 or eu-west-2"
	echo "Parameter #3 - Cluster Name"
	help
	exit 1
fi

if [ "$1" == "-l" ] ; then
	echo "Running Services"
	echo "================="
	aws ecs list-services --region $REGION --cluster $ECS_CLUSTER | jq '.serviceArns[]' | awk -F "/" '{print $2}' | tr -d '"'
	exit 0
fi

if [ "$1" == "-u" ] ; then
	if [ -z  $1 ] || [ -z $2 ] || [ -z $3 ] || [ -z $4 ]; then
        	echo "Missing Required Parameter"
        	echo "==============================="
        	echo "Parameter #2 - Region"
	        echo "Parameter #3 - Cluster Name"
	        echo "Parameter #4 - Task Count"
		echo "Parameter #5 - Service"
        	exit 1
fi
        echo "Updating Services"
        echo "================="
	update_task_count $TASK_COUNT $SERVICE
        exit 0
fi

if [ "$1" == "-n" ] ; then
        echo "Shutting all tasks down"
        echo "================="
	aws_services=$( aws ecs list-services --cluster $ECS_CLUSTER | jq '.serviceArns[]' | awk -F "/" '{print $2}' | tr -d '"')
	for key in ${aws_services[@]}; do
		echo "Updating Service: $key"
		update_task_count 0 $key
	done
        exit 0
fi

if [ "$1" == "-h" ] ; then
	help
        exit 0
fi
