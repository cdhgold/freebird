import boto3
import paramiko
def worker_handler(event, context):

    s3_client = boto3.client('s3')
    #Download private key file from secure S3 bucket
    s3_client.download_file('sapho-lamda-ssh','keys/bamboo_05_01_2018', '/tmp/bamboo_05_01_2018')

    k = paramiko.RSAKey.from_private_key_file("/tmp/bamboo_05_01_2018")
    c = paramiko.SSHClient()
    c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    host=event['IP']
    print "Connecting to " + host
    c.connect( hostname = host, username = "ec2-user", pkey = k )
    print "Connected to " + host

    commands = [
        "aws s3 cp s3:///sapho-lamda/scripts/create_alerts.sh /home/ec2-user/create_alerts.sh",
        "chmod 700 /home/ec2-user/create_alerts.sh",
        "/home/ec2-user/create_alerts.sh"
        ]
    for command in commands:
        print "Executing {}".format(command)
        stdin , stdout, stderr = c.exec_command(command)
        print stdout.read()
        print stderr.read()

    return
    {
        'message' : "Script execution completed. See Cloudwatch logs for complete output"
    }