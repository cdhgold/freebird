#!/bin/bash

for REGION in `aws ec2 describe-regions --output text | cut -f3`
do

aws cloudwatch describe-alarms --region=$REGION --state-value "INSUFFICIENT_DATA" | grep -E "AlarmName|unknown" | grep -B1 unknown | grep AlarmName | tr -d ",\"" | awk -F ":" '{print $2}' > ALARM_LIST

INSUFFICIENT_ALARMS="$(<ALARM_LIST)"

for ALARM in $INSUFFICIENT_ALARMS; do
	echo "Deleteing $REGION Terminated Instance Alarm $ALARM"
	aws cloudwatch delete-alarms --region=$REGION --alarm-names $ALARM
done

aws cloudwatch describe-alarms --region=$REGION --state-value "INSUFFICIENT_DATA" | grep -E "AlarmName|Unchecked" | grep -B1 Unchecked |grep AlarmName | tr -d ",\"" | awk -F ":" '{print $2}' > ALARM_LIST

INSUFFICIENT_ALARMS="$(<ALARM_LIST)"

for ALARM in $INSUFFICIENT_ALARMS; do
        echo "Deleteing $REGION Unchecked Alarm $ALARM"
        aws cloudwatch delete-alarms --region=$REGION --alarm-names $ALARM
done
done
