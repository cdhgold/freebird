#!/bin/bash

#Script Name: AWS EC2 Alert creation
#Script runs on one region andadds AWS Alarms for CPU Useage over 80% and for any AWS Status check failure
# requires as parameters AWS Profile, Region and ARN of the Alert Topic

##Parameters
#set -x

for REGION in `aws ec2 describe-regions --output text | cut -f3`
do
	echo "Getting Running Instance list for" $REGION
	TOPIC_ARN="arn:aws:sns:$REGION:870093347254:Outage_alert_topic"

	aws ec2 --region=$REGION describe-instances --query "Reservations[*].Instances[*].InstanceId" --filter "Name=instance-state-name,Values=running" | tr -d ",[] \"" | sed '/^$/d' > INSTANCE_LIST

	#Loop thru the file of instances and create alerts
	INSTANCES="$(<INSTANCE_LIST)" #

	for EC2_ALERT in $INSTANCES; do
		echo "Checking Instance $EC2_ALERT for alerts and creating if needed"
        	aws cloudwatch put-metric-alarm --region=$REGION --alarm-name $EC2_ALERT"_CPU_Monitor" --alarm-description "Alarm when CPU exceeds 80 percent" --metric-name CPUUtilization --namespace AWS/EC2 --statistic Average --period 300 --threshold 80 --comparison-operator GreaterThanOrEqualToThreshold  --dimensions "Name=InstanceId,Value=$EC2_ALERT" --evaluation-periods 3 --alarm-actions $TOPIC_ARN --ok-actions $TOPIC_ARN --unit Percent

		aws cloudwatch put-metric-alarm --region=$REGION --alarm-name $EC2_ALERT"_StatusCheckFailed-Alarm" --metric-name StatusCheckFailed --namespace AWS/EC2 --statistic Maximum --dimensions "Name=InstanceId,Value=$EC2_ALERT" --unit Count --period 300 --evaluation-periods 2 --threshold 1 --comparison-operator GreaterThanOrEqualToThreshold --alarm-actions $TOPIC_ARN --ok-actions $TOPIC_ARN
	done
done
